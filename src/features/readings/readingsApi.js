import {endpoint, handleErrors} from 'api/api';

export const fetchReadings = () => {
    return fetch(`${endpoint}/device`)
        .then(handleErrors)
        .then(response => response.json())
        .then(result => result.data);
};

export const fetchReading = (name) => {
    return fetch(`${endpoint}/device/${name}`)
        .then(handleErrors)
        .then(response => response.json())
        .then(result => result.data);
};

export const toggleReading = (readingName, active) => {
    const url = `${endpoint}/device/${readingName}?active=${String(active)}`;
    return fetch(url, {method: 'PATCH'})
        .then(handleErrors);
};
