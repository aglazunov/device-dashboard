import React from 'react';
import Reading from './Reading';

class KeyboardEnabledReading extends React.Component {
    constructor(props) {
        super(props);

        this.handleItemKeyDown = this.handleItemKeyDown.bind(this);
    }

    handleItemKeyDown(event) {
        const ENTER = 13;

        switch (event.keyCode) {
            case ENTER:
                (event.target === event.currentTarget) &&
                this.props.toggleReading(this.props.name, !this.props.active);
                break;
            default:
                break;
        }
    }

    render() {
        return <Reading onItemKeyDown={this.handleItemKeyDown}
                        {...this.props}/>
    }
}

export default KeyboardEnabledReading;
