import React from 'react';
import * as Status from 'features/status/statusConstants';
import './Reading.css';
import cx from 'classnames';

const Reading = ({name, status, active, unit, value, timestamp, dismissReadingStatus, onItemKeyDown, toggleReading}) => {
    const message = status.message,
        loading = status.status === Status.LOADING,
        failure = status.status === Status.FAILURE,
        success = !failure && !loading,
        zoom = String(value).length < 10;

    return (
        <div key={name} data-component="Reading"
             tabIndex={0}
             onKeyDown={onItemKeyDown}
             className={cx({
                 loading,
                 active,
                 failure,
                 success
             })}>

            <div className="activate-overlay"
                 onClick={() => toggleReading(name, true)}>
                {active ? 'Deactivate' : 'Activate'}
            </div>

            <div className="standard-box">
                <div className="header">
                    <div className="title">
                        {name}
                    </div>
                    <div className="icon-active"
                         onClick={() => toggleReading(name, false)}>
                        <i className="fas fa-signal"/>
                    </div>
                </div>
                <div className="content">
                    <div className="readings">
                        <div className={cx({value: true, zoom})} title={zoom ? null : value}>
                            {value}
                        </div>
                        <div className="unit">
                            {unit}
                        </div>
                    </div>
                    <div className="status">
                        <div className="status-icon">
                            {failure && <i className="fas fa-exclamation-circle"/>}
                            {loading && <i className="fas fa-signal"/>}
                        </div>
                        {failure && (<React.Fragment>
                            <div>
                                <span>{message}</span>
                            </div>
                            <div>
                                <a href="#dismiss" onClick={(e) => {e.preventDefault(); dismissReadingStatus(name)}}>Dismiss</a>
                            </div>
                        </React.Fragment>)}
                        {loading && <span>{message}</span>}
                    </div>
                </div>
                {success && <div className="footer">
                    <div className="footer-text">
                        {timestamp}
                    </div>
                </div>}
            </div>
        </div>);
};

export default Reading;
