import React from 'react';
import ConnectedReading from './ConnectedReading';
import './ReadingsContainer.css';

const ReadingsContainer = (props) => (<React.Fragment>
    <div data-component="ReadingsContainer">
        {props.readings.map(reading => {
            return <ConnectedReading {...reading} key={reading.name}/>
        })}
        {props.readings.length === 0 && <div>No readings to show</div>}
    </div>
</React.Fragment>);

export default ReadingsContainer;
