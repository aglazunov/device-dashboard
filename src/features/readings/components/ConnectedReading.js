import KeyboardEnabledReading from './KeyboardEnabledReading';
import {connect} from 'react-redux';
import {toggleReading, dismissReadingStatus} from '../readingsActionCreators';

const mapStateToProps = (state, ownProps) => ({
    dismissStatus: dismissReadingStatus
});

export default connect(mapStateToProps, {
    toggleReading,
    dismissReadingStatus
})(KeyboardEnabledReading);