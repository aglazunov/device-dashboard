import moment from 'moment';

const getReadingRepresentation = () => {
    const transformations = [{
        key: 'timestamp',
        transform: timestamp => moment(timestamp).calendar()
    }];

    return reading => {
        reading = {...reading};
        transformations.forEach(transformation => {
            reading[transformation.key] = transformation.transform(reading[transformation.key]);
        });
        return reading;
    };
};


export const getVisibleReadings = (state, filter) => {
    let readingRepresentation = getReadingRepresentation();
    return state.filter(reading => reading.name.toLowerCase().indexOf(filter.toLowerCase()) >= 0).map(readingRepresentation);
};

export const getReadingsStatistics = (state) => {
    const activeCount = state.filter(reading => reading.active).length;

    return {
        active: activeCount,
        inactive: state.length - activeCount,
        total: state.length
    }
};
