import {createReducer} from 'util/reducerUtils';
import * as Constants from './readingsConstants';
import * as Status from '../status/statusConstants';

const initialState = [];

const readingProcessor = (readingExchangeObject) => ({
    ...readingExchangeObject,
    status: {
        status: Status.SUCCESS
    }
});

const readingsFetched = (state, {readings}) => {
    return readings.map(readingProcessor);
};

const updateReadingStatus = (state, name, status, message) => {
    const getUpdatedReading = (reading) => ({
            ...reading,
            status: {
                status, message
            }
        }
    );
    return state.map(reading => (
        reading.name === name ? getUpdatedReading(reading) : reading
    ));
};

const updateReading = (state, {name, reading: newReading}) => {
    return state.map(reading => (
        reading.name === name ? readingProcessor(newReading) : reading
    ));
};

const readingsReducer = createReducer(initialState, {
    [Constants.TOGGLE_READING_BEGIN]: (state, {name, message}) => updateReadingStatus(state, name, Status.LOADING, message),
    [Constants.TOGGLE_READING_SUCCESS]: (state, {name}) => updateReadingStatus(state, name, Status.SUCCESS),
    [Constants.TOGGLE_READING_FAILURE]: (state, {name, message}) => updateReadingStatus(state, name, Status.FAILURE, message),

    [Constants.FETCH_READING_SUCCESS]: updateReading,
    [Constants.FETCH_READING_BEGIN]: (state, {name, message}) => updateReadingStatus(state, name, Status.LOADING, message),
    [Constants.FETCH_READING_FAILURE]: (state, {name, message}) => updateReadingStatus(state, name, Status.FAILURE, message),

    [Constants.FETCH_READINGS_SUCCESS]: readingsFetched,

    [Constants.DISMISS_READING_STATUS]: (state, {name}) => updateReadingStatus(state, name, Status.SUCCESS)
});

export default readingsReducer;
