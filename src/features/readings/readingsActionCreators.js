import * as Types from './readingsConstants';

export const fetchReadings = () => ({
    type: Types.FETCH_READINGS
});

export const fetchReading = (name) => ({
    type: Types.FETCH_READING,
    name: name
});

export const toggleReading = (name, active) => ({
    type: Types.TOGGLE_READING,
    active,
    name: name
});

export const dismissReadingStatus = (name) => ({
    type: Types.DISMISS_READING_STATUS,
    name
});
