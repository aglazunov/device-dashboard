import {
    getVisibleReadings,
    getReadingsStatistics
} from './readingsSelectors';

const sampleReading = {
    unit: 'm/s',
    value: 10,
    timestamp: +new Date()
};
const reading = (name) => ({
    ...sampleReading,
    name: name
});
const extractName = reading => reading.name;


it('filters visible readings - empty', () => {
    expect(getVisibleReadings([], '').map(extractName)).toEqual([]);
    expect(getVisibleReadings([], 'hello').map(extractName)).toEqual([]);
});

it('filters selected readings - lowercase', () => {
    const  names = ['another', 'lifetime', 'blackness', 'virtue', 'wilderness', 'creature', 'shelter', 'again', 'assured', 'always', 'steel-eyed', 'death', 'fighting'];
    const  readings = names.map(reading);

    expect(getVisibleReadings(readings, '').map(extractName)).toEqual(names);
    expect(getVisibleReadings(readings, 'tu').map(extractName)).toEqual(['virtue', 'creature']);
    expect(getVisibleReadings(readings, 'ss').map(extractName)).toEqual(['blackness', 'wilderness', 'assured']);
    expect(getVisibleReadings(readings, 'unreal filter').map(extractName)).toEqual([]);
});

it('filters selected readings - case insensitivy', () => {
    const names = ["ANotHEr", "LifEtIMe", "blaCkneSs", "viRtUE", "WiLDErNesS", "CReATUre", "sheLTER", "AgaIn", "assURed", "aLWaYS", "STEel-eyED", "dEath", "FiGHtiNG"];
    const readings = names.map(reading);
    expect(getVisibleReadings(readings, 'sS').map(extractName)).toEqual(['blaCkneSs', 'WiLDErNesS', 'assURed']);
    expect(getVisibleReadings(readings, 'Tu').map(extractName)).toEqual(['viRtUE', 'CReATUre']);
    expect(getVisibleReadings(readings, 'unreal filter').map(extractName)).toEqual([]);

    expect(getVisibleReadings(readings, '').map(extractName)).toEqual(names);
});

it('produces accurate readings statistics', () => {
    const activityMap = [
        true, false, true, false, true, true, true, false, true
    ];
    const readings = activityMap.map(active => ({
        ...sampleReading,
        active
    }));

    let statistics = getReadingsStatistics(readings);
    expect(statistics.total).toEqual(activityMap.length);
    expect(statistics.active).toEqual(activityMap.filter(d => d).length);
    expect(statistics.inactive).toEqual(activityMap.filter(d => !d).length);
});