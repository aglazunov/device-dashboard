import { put, takeEvery, all } from 'redux-saga/effects';
import * as api from './readingsApi';
import * as actions from './readingsActionCreators';
import * as Types from './readingsConstants';

const fetchReadingsBegin = () => ({
    type: Types.FETCH_READINGS_BEGIN
});

const fetchReadingsSuccess = (readings) => ({
    type: Types.FETCH_READINGS_SUCCESS,
    readings
});

const fetchReadingsFailure = (failure) => ({
    type: Types.FETCH_READINGS_FAILURE,
    failure,
    message: failure.message
});

const fetchReadingBegin = (name) => ({
    type: Types.FETCH_READING_BEGIN,
    name
});

const fetchReadingSuccess = (name, reading) => ({
    type: Types.FETCH_READING_SUCCESS,
    name,
    reading
});

const fetchReadingFailure = (name, failure) => ({
    type: Types.FETCH_READING_FAILURE,
    name,
    failure,
    message: failure.message
});

const toggleReadingBegin = (name, active) => ({
    type: Types.TOGGLE_READING_BEGIN,
    message: active ? 'Activating…' : "Deactivating…",
    name
});

// after toggling one reading we need to refresh it
const toggleReadingSuccess = (name, result) => ({
    type: Types.TOGGLE_READING_SUCCESS,
    name,
    result
});

const toggleReadingFailure = (name, active, failure) => ({
    type: Types.TOGGLE_READING_FAILURE,
    active,
    name,
    failure,
    message: failure.message
});

export function* fetchReadingsAsync() {
    yield put(fetchReadingsBegin());
    try {
        const readings = yield api.fetchReadings();
        yield put(fetchReadingsSuccess(readings));
    } catch (error) {
        yield put(fetchReadingsFailure(error));
    }
}

export function* toggleReadingAsync({name, active}) {
    yield put(toggleReadingBegin(name, active));
    try {
        yield api.toggleReading(name, active);
        yield put(toggleReadingSuccess(name));
        yield put(actions.fetchReading(name));
    } catch (error) {
        yield put(toggleReadingFailure(name, active, error));
    }
}

export function *fetchReadingAsync({name}) {
    yield put(fetchReadingBegin(name));
    try {
        const reading = yield api.fetchReading(name);
        yield put(fetchReadingSuccess(name, reading));
    } catch (error) {
        yield put(fetchReadingFailure(name, error));
    }
}

function* watchFetchReadings() {
    yield takeEvery(Types.FETCH_READINGS, fetchReadingsAsync);
}

function* watchFetchReading() {
    yield takeEvery(Types.FETCH_READING, fetchReadingAsync);
}

function* watchToggleReading() {
    yield takeEvery(Types.TOGGLE_READING, toggleReadingAsync);
}

export default function* rootSaga() {
    yield all([
        watchFetchReadings(),
        watchFetchReading(),
        watchToggleReading()
    ]);
}
