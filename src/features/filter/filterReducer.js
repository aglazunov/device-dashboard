import {createReducer} from 'util/reducerUtils';
import * as Constants from './filterConstants';

const initialState = '';

const filterReducer = createReducer(initialState, {
    [Constants.CHANGE_FILTER]: (state, {filter}) => filter
});

export default filterReducer;
