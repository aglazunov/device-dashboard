import * as Types from './filterConstants';

export const changeFilter = (filter) => ({
    type: Types.CHANGE_FILTER,
    filter
});