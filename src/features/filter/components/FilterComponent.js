import React from 'react';

const FilterComponent = (props) => (
    <input type="text"
           placeholder="Search…"
           value={props.filter}
           onChange={(event) => props.changeFilter(event.target.value)}
    />
);

export default FilterComponent;
