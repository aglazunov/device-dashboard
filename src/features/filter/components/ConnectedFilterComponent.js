import {connect} from 'react-redux';
import FilterComponent from './FilterComponent';
import * as filterActions from 'features/filter/filterActionCreators';
import {getFilter} from 'reducers/rootReducer';

const mapStateToProps = (state) => ({
    filter: getFilter(state),
});

export default connect(mapStateToProps, filterActions)(FilterComponent);
