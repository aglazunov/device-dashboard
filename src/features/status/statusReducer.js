import * as actions from '../readings/readingsConstants';
import {createReducer} from 'util/reducerUtils';
import * as Status from './statusConstants';

const initialState = {
    message: null
};

const defaultFailureMessage = 'Failed to fetch data';

export default createReducer(initialState, {
    [actions.FETCH_READINGS_BEGIN]: (state) => ({
        message: 'Loading…',
        status: Status.LOADING
    }),
    [actions.FETCH_READINGS_SUCCESS]: (state, {coins}) => ({
        status: Status.SUCCESS,
        message: null
    }),
    [actions.FETCH_READINGS_FAILURE]: (state, {message}) => ({
        status: Status.FAILURE,
        message: typeof message === 'string' ? message : defaultFailureMessage
    })
});
