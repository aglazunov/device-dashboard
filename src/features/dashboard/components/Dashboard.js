import React, {Component} from 'react';
import ReadingsContainer from 'features/readings/components/ReadingsContainer';
import ConnectedFilterComponent from 'features/filter/components/ConnectedFilterComponent';
import * as Status from 'features/status/statusConstants';
import cx from 'classnames';

import './Dashboard.css';

const refreshEnabled = false;

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.handleRefreshClick = this.handleRefreshClick.bind(this);
    }

    componentDidMount() {
        this.props.fetchReadings();
    }

    handleRefreshClick(e) {
        e.preventDefault();
        this.props.fetchReadings();
    }

    render() {
        const {status, readings, statistics} = this.props,
            success = status.status === Status.SUCCESS,
            failure = status.status === Status.FAILURE,
            loading = status.status === Status.LOADING;

        return (<div data-component="Dashboard">
            <div className="dashboard-header">
                <h3 className="title">Device Dashboard</h3>

                {success && <h4>
                    <span className="total">{statistics.total} Readings</span>
                    {' - '}
                    <span className="active">{statistics.active} Active</span>
                    {' - '}
                    <span className="inactive">{statistics.inactive} Inactive</span>
                </h4>}
            </div>

            {success && <div>
                <div className="search-wrapper">
                    <ConnectedFilterComponent/>
                </div>

                <div className="actions-wrapper">
                    {refreshEnabled && <a href="#refresh" onClick={this.handleRefreshClick}>Refresh</a>}
                </div>

                <ReadingsContainer readings={readings}/>
            </div>}

            {!success && <div className={cx('status-container', {success, loading, failure})}>
                <div className="status-icon">
                    {failure && <i className="fas fa-exclamation-circle"/>}
                    {loading && <i className="fas fa-rocket"/>}
                </div>
                <span className="status-message">{status.message}</span>
            </div>}
        </div>);
    }
}

export default Dashboard;
