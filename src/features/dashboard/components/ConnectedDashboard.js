import {connect} from 'react-redux';
import Dashboard from './Dashboard';
import {getVisibleReadings, getStatus, getReadingsStatistics} from 'reducers/rootReducer';
import * as actions from 'features/readings/readingsActionCreators';

const mapStateToProps = (state) => ({
    readings: getVisibleReadings(state),
    statistics: getReadingsStatistics(state),
    status: getStatus(state)
});

export default connect(mapStateToProps, actions)(Dashboard);
