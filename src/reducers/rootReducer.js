import {combineReducers} from 'redux';
import readingsReducer from 'features/readings/readingsReducer';
import filterReducer from 'features/filter/filterReducer';
import statusReducer from 'features/status/statusReducer';
import * as readingSelectors from 'features/readings/readingsSelectors';

const rootReducer = combineReducers({
    readings: readingsReducer,
    filter: filterReducer,
    status: statusReducer
});

export const getReadings = state => state.readings;
export const getStatus = state => state.status;
export const getFilter = state => state.filter;

export const getVisibleReadings = (state) =>
    readingSelectors.getVisibleReadings(getReadings(state), state.filter);

export const getReadingsStatistics =(state) =>
    readingSelectors.getReadingsStatistics(getReadings(state));

export default rootReducer;
