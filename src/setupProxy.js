const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(proxy('/api', {
        target: 'http://localhost:7701/',
        pathRewrite: {
            '^/api/' : '/',     // rewrite path
        }
    }));
};