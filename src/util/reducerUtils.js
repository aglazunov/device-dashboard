export function createReducer(initialState, fnMap) {
    return (state = initialState, payload) => {
        const handler = fnMap[payload.type];
        return handler ? handler(state, payload) : state;
    };
}