import React from 'react';
import ConnectedDashboard from 'features/dashboard/components/ConnectedDashboard';
import './app.css';

const App = (props) => <ConnectedDashboard/>;
export default App;
