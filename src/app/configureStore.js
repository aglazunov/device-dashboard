import {createStore, applyMiddleware} from 'redux';
import rootReducer from 'reducers/rootReducer';
import createLogger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'features/readings/readingsSagas';

/*const addPromiseSupportToDispatch = store => next => action => {
    if (typeof action.then === 'function') {
        return action
            .then(next)
            .catch(next);
    }
    return next(action);
};

const addBatchSupportToDispatch = store => next => action => {
    if (action instanceof Array) {
        return action.map(action => next(action));
    }
    return next(action);
};*/

const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
    const middlewares = [
        sagaMiddleware,
        createLogger
    ];
    const store = createStore(
        rootReducer,
        composeWithDevTools(applyMiddleware(...middlewares))
    );

    sagaMiddleware.run(rootSaga);

    if (process.env.NODE_ENV !== 'production') {
        if (module.hot) {
            module.hot.accept('reducers/rootReducer', () => {
                const newRootReducer = require('reducers/rootReducer').default;
                store.replaceReducer(newRootReducer)
            });
        }
    }

    return store;
};

export default configureStore;
