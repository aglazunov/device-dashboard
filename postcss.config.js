module.exports = {
    plugins: [
        require('postcss-import'),
        require('postcss-preset-env')({
            stage: 1,
        }),
        require('postcss-simple-vars'),
        require('postcss-nested'),
        require('autoprefixer'),
    ],
};