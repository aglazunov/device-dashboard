# Solution for Relayr Frontend Challenge

## Problem

Build a React-based frontend interface for displaying data from and interacting with an IoT device with the following 
functionality:

* **Retrieve the device's state** from the backend.
* **Show each of the device's readings**: name, unit, value, timestamp, and active status.
* **Show a counter** showing how many readings are active and how many are inactive.
* **Implement a search input** that filters visible readings by name.
* A user should also be able to **toggle the active status of each reading** by making the proper requests to the backend. 
After successfully changing the status on the backend, the UI should display the updated state of the active counter.

[See full task description here](Task.md)

## Note

The API on the backend is implemented to simulate long delayed responses and failures randomly.

## Demo

You can look at the solution in action at [device-dashboard.glazunov.eu](http://device-dashboard.glazunov.eu/).

## Running solution

run `yarn` and then `yarn start`.

